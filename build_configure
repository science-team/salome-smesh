#!/bin/bash
# Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Tool for updating list of .in file for the SALOME project 
# and regenerating configure script
# Author : Marc Tajchman - CEA
# Date : 10/10/2002
# Modified by : Alexander BORODIN (OCN) - autotools usage
# $Header: /home/server/cvs/SMESH/SMESH_SRC/build_configure,v 1.15.2.3.4.2.8.1 2012-04-13 09:30:59 vsr Exp $
#
ORIG_DIR=`pwd`
CONF_DIR=`echo $0 | sed -e "s,[^/]*$,,;s,/$,,;s,^$,.,"`

########################################################################
# Test if the KERNEL_ROOT_DIR is set correctly

if test ! -d "${KERNEL_ROOT_DIR}"; then
    echo "failed : KERNEL_ROOT_DIR variable is not correct !"
    exit
fi

# Test if the KERNEL_SRC is set correctly

#if test ! -d "${KERNEL_SRC}"; then
#    echo "failed : KERNEL_SRC variable is not correct !"
#    exit
#fi

########################################################################
# Test if the MED_ROOT_DIR is set correctly

if test ! -d "${MED_ROOT_DIR}"; then
    echo "failed : MED_ROOT_DIR variable is not correct !"
    exit
fi

########################################################################
# Test if the GEOM_ROOT_DIR is set correctly

if test ! -d "${GEOM_ROOT_DIR}"; then
    echo "failed : GEOM_ROOT_DIR variable is not correct !"
    exit
fi


cd ${CONF_DIR}
ABS_CONF_DIR=`pwd`

#######################################################################

# ____________________________________________________________________
# aclocal creates the aclocal.m4 file from the standard macro and the
# custom macro embedded in the directory adm_local/unix/config_files,
# KERNEL salome_adm/unix/config_files, GEOM and MED adm_local/unix/config_files
# directories.
# output:
#   aclocal.m4
#   autom4te.cache (directory)
echo "====================================================== aclocal"

if test -d "${GUI_ROOT_DIR}"; then
  aclocal -I adm_local/unix/config_files \
          -I ${KERNEL_ROOT_DIR}/salome_adm/unix/config_files \
          -I ${GUI_ROOT_DIR}/adm_local/unix/config_files \
          -I ${MED_ROOT_DIR}/adm_local/unix/config_files \
          -I ${GEOM_ROOT_DIR}/adm_local/unix/config_files || exit 1
else
  aclocal -I adm_local/unix/config_files \
          -I ${KERNEL_ROOT_DIR}/salome_adm/unix/config_files \
          -I ${MED_ROOT_DIR}/adm_local/unix/config_files \
          -I ${GEOM_ROOT_DIR}/adm_local/unix/config_files || exit 1
fi


# ____________________________________________________________________
# libtoolize creates some configuration files (ltmain.sh,
# config.guess and config.sub). It only depends on the libtool
# version. The files are created in the directory specified with the
# AC_CONFIG_AUX_DIR(<mydir>) tag (see configure.ac).
# output:
#   adm_local/unix/config_files/config.guess
#   adm_local/unix/config_files/config.sub
#   adm_local/unix/config_files/ltmain.sh
echo "==================================================== libtoolize"

libtoolize --force --copy --automake || exit 1

# ____________________________________________________________________
# autoconf creates the configure script from the file configure.ac (or
# configure.in if configure.ac doesn't exist)
# output:
#   configure
echo "====================================================== autoconf"

autoconf

# ____________________________________________________________________
# automake creates some scripts used in building process
# (install-sh, missing, ...). It only depends on the automake
# version. The files are created in the directory specified with the
# AC_CONFIG_AUX_DIR(<mydir>) tag (see configure.ac). This step also
# creates the Makefile.in files from the Makefile.am files.
# output:
#   adm_local/unix/config_files/compile
#   adm_local/unix/config_files/depcomp
#   adm_local/unix/config_files/install-sh
#   adm_local/unix/config_files/missing
#   adm_local/unix/config_files/py-compile
#   Makefile.in (from Makefile.am)
echo "====================================================== automake"

automake --copy --gnu --add-missing
